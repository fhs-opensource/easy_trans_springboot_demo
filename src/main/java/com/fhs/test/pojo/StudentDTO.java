package com.fhs.test.pojo;

import com.fhs.core.trans.anno.UnTrans;
import com.fhs.core.trans.constant.UnTransType;
import lombok.Data;

/**
 * 反向翻译测试专用
 */
@Data
public class StudentDTO {
    private String schoolName;
    @UnTrans(type= UnTransType.SIMPLE,tableName = "school",refs = "schoolName",columns = "school_name",uniqueColumn = "id")
    private Integer schoolId;
    private String studentNo;
    private String studentName;
    private String studentAge;
    @UnTrans(type= UnTransType.SIMPLE,tableName = "user",refs = {"studentName","studentAge"},columns = {"name","age"},uniqueColumn = "user_id")
    private Integer userId;

    private String genderName;
    @UnTrans(type=UnTransType.DICTIONARY,dict="sex",refs = {"genderName"})
    private Integer gender;
}
